package com.gta.kafka.consumer;

// http://cloudurable.com/blog/kafka-tutorial-kafka-consumer/index.html

/*
 * java -jar /Users/paoloscarpino/git/kafkaconsumer/target/KafkaConsumer-0.0.1-SNAPSHOT-shaded.jar group_one 127.0.0.1:9092 test2 200
 */

public class MainConsumer {
    
    public static void main(String... args) throws Exception {
    		String jarFileName = new java.io.File(MainConsumer.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
    		
    		if (args.length == 4) {
    			System.out.println("groupId: " + args[0]);
    			System.out.println("servers: " + args[1]);
    			System.out.println("topics: " + args[2]);
    			System.out.println("topics: " + args[3]);
    			
    			KafkaConsumer consumer = new KafkaConsumer( args[0], args[1], args[2], new Integer(args[3]).intValue() );
    			
    			try {
    				consumer.runConsumer();
    			}catch(Exception e) {
    				e.printStackTrace();
    			}finally {
    				consumer.stopConsumer();
    			}
    			
    		}else {
    			System.err.printf("Usage: java -jar %s <groupId> <servers> <topics> <BatchSize> \n", jarFileName);
    			System.err.printf("\tE.g.: java -jar %s group_one 127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094 test1,test2,test3 200 \n", jarFileName);
    		}
    }
}
