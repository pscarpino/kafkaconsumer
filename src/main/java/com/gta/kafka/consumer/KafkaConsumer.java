package com.gta.kafka.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

public class KafkaConsumer {

	private Consumer<String, String> consumer;
	private int minBatchSize;
	
	public KafkaConsumer( String groupId, String servers, String topics, int minBatchSize ) {
		super();
		if(minBatchSize > 0)
			this.minBatchSize = minBatchSize;
		else
			this.minBatchSize = 1;
		createConsumer( groupId, servers, topics );
	}
	
	private void createConsumer(final String groupId, final String servers, final String topics) {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Create the consumer using props.
        consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(props);

        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(topics));
    }
	
	public void runConsumer() throws Exception {
		List<ConsumerRecord<String, String>> buffer = new ArrayList<ConsumerRecord<String, String>>();
		
		while (true) {
            final ConsumerRecords<String, String> consumerRecords = consumer.poll(100);
            
            for (TopicPartition partition : consumerRecords.partitions()) {
                List<ConsumerRecord<String, String>> partitionRecords = consumerRecords.records(partition);
                partitionRecords.forEach(record -> {
                    System.out.println(partition.partition() + ": " + record.offset() + ": " + record.value());
                    buffer.add(record);
                });
            }
            if ( buffer.size() >= minBatchSize ) {
                // Some operations - e.g. insertIntoDb(buffer);
                consumer.commitSync();
                buffer.clear();
            }
        }
    }
	
	public void stopConsumer() throws Exception {
		consumer.close();
	}
}
